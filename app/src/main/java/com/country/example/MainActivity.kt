package com.country.example


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.country.emoji.model.CountryListArguments
import com.country.emoji.ui.list.CountryActivityContract
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cvv_choose_country.setDefaultCountry(Locale.getDefault())
        cvv_choose_country.setOnClickListener {
            openChooseCountryListActivity.launch(
                CountryListArguments(
                    //dayNightMode = MODE_NIGHT_YES,
                    enableSearch = cb_search.isChecked,
                    displayCountryCode = false,
                    excludeMode = cb_exclude_mode.isChecked,
                    filter = arrayListOf(
                        Locale.FRANCE.country,
                        Locale.ENGLISH.country,
                        Locale.JAPAN.country
                    ),
                    country = cvv_choose_country.countryCode
                )
            )
        }

        ed_phone.formatNumber = cvv_choose_country.value.formatMask
    }

    private val openChooseCountryListActivity =
        registerForActivityResult(CountryActivityContract()) { result ->
            result?.let {
                cvv_choose_country.value = result
                ed_phone.formatNumber = result.formatMask
            }
        }


}
