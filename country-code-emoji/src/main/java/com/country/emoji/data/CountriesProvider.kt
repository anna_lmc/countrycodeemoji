package com.country.emoji.data

import android.content.res.Resources
import com.country.emoji.R
import com.country.emoji.model.Country
import com.country.emoji.extensions.moveToFirstIf
import com.country.emoji.extensions.toEmojiFrag
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import java.io.*
import java.lang.reflect.Type
import java.text.NumberFormat
import java.util.*
import java.util.stream.Collectors

class CountriesProvider(private val dispatcher: CoroutineDispatcher = Dispatchers.Main) {

    /**
     * @param iso Country ios code
     * @return Located country name
     */
    private fun getLocalName(iso: String): String {
        val locale = Locale(Locale.getDefault().country, iso)
        return locale.displayCountry
    }

    /**
     * @param code Country ios code
     * @return Located phone code (actual for arabic)
     */
    private fun getLocalNumber(code: Int): String {
        val nf: NumberFormat = NumberFormat.getInstance(Locale.getDefault())
        return nf.format(code)
    }

    /**
     * Parsing JSON file from raw which is got via Resources
     * Init country name by current device locale
     * Find emoji symbol by iso
     * Filter countries via list of iso
     * @param isExcludeMode enable/disable support or exclude filter mode
     * @param resources
     * @return list of Country
     */
    suspend fun fetchCountries(
        resources: Resources,
        currentCountry: String?,
        filter: List<String>,
        isExcludeMode: Boolean
    ): List<Country> = withContext(dispatcher) {
        val output = loadDataFromFile(resources).stream()
            .filter { e -> if (isExcludeMode) !filter.contains(e.iso) else filter.contains(e.iso) }
            .collect(Collectors.toList())
        output.forEach {
            it.flag = it.iso.toEmojiFrag()
            it.titleLocal = getLocalName(it.iso)
            it.codeLocal = getLocalNumber(it.countryCode)
        }
        //sort list by localized name
        output.sortedBy { it.titleLocal.toLowerCase(Locale.getDefault()) }
        //move current country to first position
        output.moveToFirstIf { item -> item.iso == currentCountry }
    }


    /**
     * Parsing JSON file from raw which is got via Resources
     * Init country name by current device locale

     * Find emoji symbol by iso
     * @param resources
     * @return list of Country
     */
    suspend fun fetchCountries(resources: Resources): List<Country> = withContext(dispatcher) {
        val countries = loadDataFromFile(resources)
        for (country in countries) {
            country.flag = country.iso.toEmojiFrag()
            country.titleLocal = getLocalName(country.iso)
        }
        countries.sortedBy { it.titleLocal.toLowerCase(Locale.getDefault()) }
    }

    /**
     * Parsing JSON file from raw which is got via Resources
     * Init country name by current device locale
     * */
    private fun loadDataFromFile(resources: Resources): List<Country> {
        val countriesData = resources.openRawResource(R.raw.countries)
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        countriesData.use { stream ->
            val reader: Reader = BufferedReader(InputStreamReader(stream, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        }
        val countriesJsonArray = JSONArray(writer.toString())
        val listType: Type = object : TypeToken<List<Country>>() {}.type
        return Gson().fromJson(countriesJsonArray.toString(), listType)
    }


    /**
     * @param resources
     * @param code Country ISO code
     * @return Country model
     */
    fun findCountryByCode(resources: Resources, code: String): Country? {
        val countries = loadDataFromFile(resources)
        val country = countries.find { country -> country.iso == code }
        country?.let {
            country.flag = country.iso.toEmojiFrag()
            country.codeLocal = getLocalNumber(country.countryCode)
            country.titleLocal = getLocalName(country.iso)

        }
        return country
    }


}