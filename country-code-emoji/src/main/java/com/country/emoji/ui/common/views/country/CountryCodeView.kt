package com.country.emoji.ui.common.views.country

import android.content.Context
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.country.emoji.R
import com.country.emoji.data.CountriesProvider
import com.country.emoji.model.Country
import com.country.emoji.extensions.toEmojiFrag
import kotlinx.android.synthetic.main.view_country_code.view.*
import java.util.*

class CountryCodeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : ConstraintLayout(context, attrs, defStyle, defStyleRes) {

    private var mode: Int = 3
    private var provider: CountriesProvider =
        CountriesProvider()

    init {

        LayoutInflater.from(context)
            .inflate(R.layout.view_country_code, this, true)

        attrs?.let { attr: AttributeSet ->

            val typedArray = context.obtainStyledAttributes(
                attr,
                R.styleable.CountryCodeView, 0, 0
            )

            val textSize = convertPixelsToDp(typedArray.getDimension(R.styleable.CountryCodeView_ccv_textSize, 14f), context)
            if (textSize > 0) {
                tv_ccp_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
            }

            val flagSize = convertPixelsToDp(typedArray.getDimension(R.styleable.CountryCodeView_ccv_flagSize, 16f), context)
            if (flagSize > 0) {
                tv_ccp_flag.setTextSize(TypedValue.COMPLEX_UNIT_SP, flagSize);

            }

            val showArrow = typedArray
                .getBoolean(
                    R.styleable
                        .CountryCodeView_ccv_showArrow,
                    true
                )

            if (!showArrow) {
                tv_ccp_flag.setCompoundDrawables(null, null, null, null);
                tv_ccp_flag.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }


            initDefaultCountry(typedArray.getString(R.styleable.CountryCodeView_ccv_defaultCountry))


            mode = typedArray
                .getInt(
                    R.styleable
                        .CountryCodeView_ccv_mode, mode
                )



            typedArray.recycle()
        }
    }

    private fun initDefaultCountry(default : String?){
        var locale : Locale? = null
        default?.let {
            locale = Locale(it)
        }
        locale?.let {
            if (it.country.isNullOrEmpty()){
                setDefaultCountry(it)
                return@let
            }
        }
        setDefaultCountry(Locale.getDefault())
    }

    fun convertPixelsToDp(px: Float, context: Context): Float {
        return px / (context.resources
            .displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }


    fun setDefaultCountry(locale: Locale) {
        provider.findCountryByCode(context.resources, locale.country)?.let {
            value = it
        }
    }

    private lateinit var country: Country


    var value: Country
        get() = country
        set(value) {
            country = value
            tv_ccp_flag.text = when (mode) {
                2 -> ""
                else -> value.iso.toEmojiFrag()
            }
            tv_ccp_text.text = when (mode) {
                1 -> ""
                else -> "+${value.codeLocal}"
            }
        }

    val countryCode: String
        get() = country.iso

}