package com.country.emoji.ui.list

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.country.emoji.R
import com.country.emoji.model.Country
import kotlinx.android.synthetic.main.rcl_item_country.view.*


class CountriesAdapter(
    private val items: MutableList<Country>,
    private var selected: String,
    private val displayCountryCode : Boolean,
    private var onItemClick: ((Country) -> Unit)?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private var filteredCountry: MutableList<Country> = mutableListOf()

    init {
        filteredCountry.addAll(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.rcl_item_country,
                parent,
                false
            )
        )
    }

    fun update(newList: List<Country>) {
        items.clear()
        filteredCountry.clear()
        items.addAll(newList)
        filteredCountry.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount() = filteredCountry.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (filteredCountry[position]).let {
            (holder as PostHolder).bind(it, selected)
        }
    }


    inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(filteredCountry[adapterPosition])
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(country: Country, selected: String) {

            itemView.tv_rcl_country_name.text =
                "${country.flag} ${country.titleLocal}"
            if (displayCountryCode) {
                itemView.tv_rcl_country_code.visibility = View.VISIBLE
                itemView.tv_rcl_country_code.text = "+${country.codeLocal}"
            }else{
                itemView.tv_rcl_country_code.visibility = View.GONE
            }
            val typeface = if (country.iso == selected) Typeface.BOLD else Typeface.NORMAL
            itemView.tv_rcl_country_code.setTypeface(
                null,
                typeface
            )
            itemView.tv_rcl_country_name.setTypeface(
                null,
                typeface
            )
        }
    }

    fun search(query: String?) {
        filteredCountry.clear()
        if (query.isNullOrEmpty())
            filteredCountry.addAll(items)
        else {
            val filtered = items.filter {
                (it.titleLocal.contains(query, true))
            }
            filteredCountry.addAll(filtered)
        }
        notifyDataSetChanged()
    }

    private fun findCountryByCode(): Country? =
        filteredCountry.find { country ->
            country.iso == selected
        }

    fun getSelectedPosition(): Int {
        findCountryByCode()?.let {
            return filteredCountry.indexOf(it)
        }
        return -1
    }


}