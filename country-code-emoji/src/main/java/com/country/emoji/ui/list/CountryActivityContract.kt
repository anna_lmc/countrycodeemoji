package com.country.emoji.ui.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.country.emoji.model.Country
import com.country.emoji.model.CountryListArguments

class CountryActivityContract : ActivityResultContract<CountryListArguments, Country?>() {

    override fun createIntent(context: Context, params: CountryListArguments): Intent {
        return Intent(context, ChooseCountryListActivity::class.java).apply {
            putExtra(ChooseCountryListActivity.PARAMS, params)
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Country? {
        val data = intent?.getParcelableExtra<Country>(ChooseCountryListActivity.COUNTRY)
        return if (resultCode == Activity.RESULT_OK && data != null) data
        else null
    }
}