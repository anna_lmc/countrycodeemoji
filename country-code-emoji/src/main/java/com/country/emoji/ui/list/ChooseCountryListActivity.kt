package com.country.emoji.ui.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.DividerItemDecoration
import com.country.emoji.R
import com.country.emoji.data.CountriesProvider
import com.country.emoji.model.CountryListArguments
import com.country.emoji.ui.base.BaseScopeActivity
import com.country.emoji.ui.common.DebounceQueryTextListener
import kotlinx.android.synthetic.main.activity_base_ui_counry_list.*
import kotlinx.android.synthetic.main.layout_action_bar.*
import kotlinx.coroutines.launch
import java.util.*


class ChooseCountryListActivity : BaseScopeActivity() {

    companion object {
        const val COUNTRY = "key_country"
        const val PARAMS = "key_params"
    }

    private lateinit var adapter: CountriesAdapter

    private val countriesProvider = CountriesProvider()
    private lateinit var parameters: CountryListArguments

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parameters = intent.getParcelableExtra(PARAMS) ?: CountryListArguments()
        AppCompatDelegate.setDefaultNightMode(parameters.dayNightMode)
        setContentView(R.layout.activity_base_ui_counry_list)
        setSupportActionBar(toolbar)
        supportActionBar?.let { actionBar ->
            actionBar.setDisplayShowHomeEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowTitleEnabled(true)
            parameters.title?.let { title ->
                actionBar.title = title
            }
        }

        rcl_activity_country_list.setHasFixedSize(true)
        rcl_activity_country_list.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        val country = parameters.country ?: Locale.getDefault().country
        adapter = CountriesAdapter(mutableListOf(), country, parameters.displayCountryCode) {
            val intent = Intent()
            intent.putExtra(COUNTRY, it)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        rcl_activity_country_list.adapter = adapter

        loadData()
    }

    private fun loadData() = launch {
        val country = parameters.country ?: Locale.getDefault().country
        val countries = countriesProvider.fetchCountries(
            resources,
            country,
            parameters.filter,
            parameters.excludeMode
        )
        adapter.update(countries)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        if (parameters.enableSearch) {
            inflater.inflate(R.menu.menu_counry_list, menu)
            menu?.let {
                val searchView =
                    it.findItem(R.id.item_menu_act_base_ui_country_list_search).actionView as SearchView
                searchView.queryHint = getString(R.string.search_title)
                searchView.setOnQueryTextListener(DebounceQueryTextListener(
                    this@ChooseCountryListActivity.lifecycle
                ) { newText ->
                    newText?.let { query ->
                        adapter.search(query)
                    }
                })
            }
        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }



}
