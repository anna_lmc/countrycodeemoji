package com.country.emoji.model

import android.os.Parcelable
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CountryListArguments(
        val dayNightMode: Int = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM,
        val enableSearch: Boolean = true,
        val displayCountryCode : Boolean = true,
        val title: String? = null,
        val filter: List<String> = arrayListOf(),
        val excludeMode: Boolean = true,
        val country: String? = null
) : Parcelable